def print_board(entries): #Run this through debugger to understand what it does
    line = "+---+---+---+"
    output = line
    n = 0 
    for entry in entries:
        if n % 3 == 0: # a % means a divisible by some number creates a remainder integer
            output = output + "\n| " # 1,4,7
        else:
            output = output + " | "
        output = output + str(entry) #This is where its adding either the X or the 5
        if n % 3 == 2:
            output = output + " |\n" # 3,6,9
            output = output + line
        n = n + 1
    print(output)
    print()
    
def game_over(board,winner,win_location):
    print("game over")
    print_board(board) #this prints the winning board!!!!
    print(f"The winner is {winner}, by way of {win_location}!")
    exit()


''' This is what Matthew had instead for the bottom functions:
def is_row_winner(board, row):
    index = row - 1
    if board[index] == board[index+1] and board[index+1] == board[index+2]:
        return True
    else:
        return False
'''

def is_row_winner(board,row_number):
    if row_number == 1 and board[0] == board[1] and board[1] == board[2]:
        return True
    elif row_number == 2 and board[3] == board[4] and board[4] == board[5]:
        return True
    elif row_number == 3 and board[6] == board[7] and board[7] == board[8]:
        return True
    return False #if you don't reference a boolean, it can be "none" which is null which is neither false nor true.

def is_column_winner(board,column_number):
    if column_number == 1 and board[0] == board[3] and board[3] == board[6]:
        return True    
    elif column_number == 2 and board[1] == board[4] and board[4] == board[7]:
        return True
    elif column_number == 3 and board[2] == board[5] and board[5] == board[8]:
        return True
    return False

def is_diagonal_winner(board,diagonal_number):
    if diagonal_number == 1 and board[0] == board[4] and board[4] == board[8]:
        return True    
    elif diagonal_number == 2 and board[2] == board[4] and board[4] == board[6]:
        return True
    return False

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10): # For is not a function, it's a built in conditional
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player


    if is_row_winner(board,1):
        game_over(board,current_player,"top row")

    elif is_row_winner(board,2):
        game_over(board,current_player,"middle row")

    elif is_row_winner(board,3):
        game_over(board,current_player,"bottom row")

    elif is_column_winner(board,1):
        game_over(board,current_player,"left column")

    elif is_column_winner(board,2):
        game_over(board,current_player,"middle column")

    elif is_column_winner(board,3):
        game_over(board,current_player,"right column")
        
    elif is_diagonal_winner(board,1):
        game_over(board,current_player,"left diagonal")
        
    elif is_diagonal_winner(board,2):
        game_over(board,current_player,"right diagonal")

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"
print("It's a tie!")
